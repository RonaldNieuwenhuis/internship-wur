"""


"""
import sys, os, itertools, csv
from Bio import SeqIO



def parse_pacasus_output(read_bin):
    """Parses the fasta output returned by Pacasus and returns a bin with the true/false
    positives/negatives."""
    counts_bin = {'Total_reads': 0,
                  'False_positive': 0,
                  'False_negative': 0,
                  'True_positive': 0,
                  'True_negative': 0,
                  'Simple_wrong': 0,
                  'Simple_right': 0,
                  'Multiple_wrong': 0,
                  'Multiple_right': 0,
                  'Normal_wrong': 0,
                  'Normal_right': 0,
                  'Invrep_wrong': 0,
                  'Invrep_right': 0
                  }
    simple_offset = []
    multiple_offset = []
    invrep_offset = []
    normal_offset = []
    leftover = {}
    for read_id, value in read_bin.items():
        value = sorted(value, key=len, reverse=True)
        features = value[0].split('_')
        number_of_splits = len(features) - 14
        category = features[2]
        counts_bin['Total_reads'] += len(value)
        if category == 'simple':
            if number_of_splits == 1:
                counts_bin['True_positive'] += 1
                counts_bin['Simple_right'] += 1
            else:
                offset = number_of_splits - 1
                simple_offset.append(offset)
                if offset < 0:
                    counts_bin['False_negative'] += 1
                elif offset > 0:
                    counts_bin['True_positive'] += 1
                    counts_bin['False_positive'] += offset
                counts_bin['Simple_wrong'] += 1
                leftover[read_id] = value
        elif category == 'multiple':
            if number_of_splits == int(features[5]):
                counts_bin['True_positive'] += number_of_splits
                counts_bin['Multiple_right'] += 1
            else:
                offset = number_of_splits - int(features[5])
                multiple_offset.append(offset)
                if offset < 0:
                    good_cuts = number_of_splits
                    counts_bin['True_positive'] += good_cuts
                    counts_bin['False_negative'] += offset/-1
                elif offset > 0:
                    counts_bin['True_positive'] += number_of_splits
                    counts_bin['False_positive'] += offset
                counts_bin['Multiple_wrong'] += 1
                leftover[read_id] = value
        elif category == 'invrep':
            if number_of_splits == 0:
                counts_bin['True_negative'] += 1
                counts_bin['Invrep_right'] += 1
            else:
                offset = number_of_splits
                invrep_offset.append(offset)
                counts_bin['False_positive'] += offset
                counts_bin['Invrep_wrong'] += 1
                leftover[read_id] = value
        elif category == 'normal':
            if number_of_splits == 0:
                counts_bin['True_negative'] += 1
                counts_bin['Normal_right'] += 1
            else:
                offset = number_of_splits
                normal_offset.append(offset)
                counts_bin['False_positive'] += offset
                counts_bin['Normal_wrong'] += 1
                leftover[read_id] = value
        else:
            print(read_id, value)
    return counts_bin, leftover, [normal_offset, simple_offset, multiple_offset, invrep_offset]


def parse_pacasus_output_2(read_bin):
    """Parses the fasta output returned by Pacasus and returns a bin with the true/false
    positives/negatives.This treats the INVREP class the same as simple and multiple, so it
    should be cut."""
    counts_bin = {'Total_reads': 0,
                  'False_positive': 0,
                  'False_negative': 0,
                  'True_positive': 0,
                  'True_negative': 0,
                  'Simple_wrong': 0,
                  'Simple_right': 0,
                  'Multiple_wrong': 0,
                  'Multiple_right': 0,
                  'Normal_wrong': 0,
                  'Normal_right': 0,
                  'Invrep_wrong': 0,
                  'Invrep_right': 0
                  }
    simple_offset = []
    multiple_offset = []
    invrep_offset = []
    normal_offset = []
    leftover = {}
    for read_id, value in read_bin.items():
        value = sorted(value, key=len, reverse=True)
        features = value[0].split('_')
        number_of_splits = len(features) - 14
        category = features[2]
        counts_bin['Total_reads'] += len(value)

        if category == 'simple':
            if number_of_splits == 1:
                counts_bin['True_positive'] += 1
                counts_bin['Simple_right'] += 1
            else:
                offset = number_of_splits - 1
                simple_offset.append(offset)
                if offset < 0:
                    counts_bin['False_negative'] += 1
                elif offset > 0:
                    counts_bin['True_positive'] += 1
                    counts_bin['False_positive'] += offset
                counts_bin['Simple_wrong'] += 1
                leftover[read_id] = value
        elif category == 'multiple':
            if number_of_splits == int(features[5]):
                counts_bin['True_positive'] += number_of_splits
                counts_bin['Multiple_right'] += 1
            else:
                offset = number_of_splits - int(features[5])
                multiple_offset.append(offset)
                if offset < 0:
                    good_cuts = number_of_splits
                    counts_bin['True_positive'] += good_cuts
                    counts_bin['False_negative'] += offset/-1
                elif offset > 0:
                    counts_bin['True_positive'] += number_of_splits
                    counts_bin['False_positive'] += offset
                counts_bin['Multiple_wrong'] += 1
                leftover[read_id] = value
        elif category == 'invrep':
            if number_of_splits == 1:
                counts_bin['True_positive'] += 1
                counts_bin['Invrep_right'] += 1
            else:
                offset = number_of_splits - 1
                invrep_offset.append(offset)
                if offset < 0:
                    counts_bin['False_negative'] += 1
                elif offset > 0:
                    counts_bin['True_positive'] += 1
                    counts_bin['False_positive'] += offset
                counts_bin['Invrep_wrong'] += 1
                leftover[read_id] = value
        elif category == 'normal':
            if number_of_splits == 0:
                counts_bin['True_negative'] += 1
                counts_bin['Normal_right'] += 1
            else:
                offset = number_of_splits
                normal_offset.append(offset)
                counts_bin['False_positive'] += offset
                counts_bin['Normal_wrong'] += 1
                leftover[read_id] = value
        else:
            print(read_id, value)
    return counts_bin, leftover, [normal_offset, simple_offset, multiple_offset, invrep_offset]




def bin_by_readid(input_file):
    """Bin the fasta headers by readid and return it as dict."""
    read_bin = {}
    for record in SeqIO.parse(input_file, 'fasta'):
        features = record.id.split('_')
        read_id = features[1]
        if read_id in read_bin:
            read_bin[read_id].append(record.id)
        else:
            read_bin[read_id] = [record.id]
    return read_bin


def main():
    input_file = str(sys.argv[1])
    read_bin = bin_by_readid(input_file)
    read_bin_ids = sorted(read_bin)
    missing_ids = []
    for i in range(1, 25000):
        if str(i) not in read_bin_ids:
            missing_ids.append(i)
    output, leftover, offsets_list = parse_pacasus_output(read_bin)
    output['Not_represented'] = len(missing_ids)
    # output_string = '{},{},{},{},{},{},{},{},{},{},{}' \
    #                 ',{},{},{},{}'.format(
    #                                       os.path.basename(input_file).split('.fa')[0],
    #                                       str(output['True_positive']),
    #                                       str(output['True_negative']),
    #                                       str(output['False_positive']),
    #                                       str(output['False_negative']),
    #                                       str(output['Not_represented']),
    #                                       str(output['Total_reads']),
    #                                       str(output['Normal_wrong']),
    #                                       str(output['Normal_right']),
    #                                       str(output['Simple_wrong']),
    #                                       str(output['Simple_right']),
    #                                       str(output['Multiple_wrong']),
    #                                       str(output['Multiple_right']),
    #                                       str(output['Invrep_wrong']),
    #                                       str(output['Invrep_right'])
    #                                       )
    for tup in itertools.zip_longest(offsets_list[0], offsets_list[1], offsets_list[2],
                                     offsets_list[3], fillvalue=''):
        output_string = '{},{},{},{},{}'.format(os.path.basename(input_file).split('.fa')[0],
                                                str(tup[0]),
                                                str(tup[1]),
                                                str(tup[2]),
                                                str(tup[3]))
        print(output_string)

    print(output_string)


if __name__ == "__main__":
    main()
