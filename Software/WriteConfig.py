"""
Test number;Gap;Mismatch;Match;Minimum;Lower Limit;Filter Factor;Query Coverage;Query Identity;Relative Score;Base Score;Query Coverage Slice;Minimum Read Length;
"""
import csv, sys


def main():
    with open('/home/ronald/Documents/internship-wur/Docs/Extra_experiments.csv') as file:
        csv_reader = csv.reader(file, delimiter=';')
        for l in csv_reader:
            exp_numb = int(l[0].split('_')[0])
            if exp_numb != 8:
                ds = str(exp_numb)
            else:
                ds = '8'
            for i in [2000, 5000, 7500, 10000, 15000, 25000]:
                with open('/home/ronald/Documents/internship-wur/Tmp/Bash_files/' + str(l[0]) + '_' + str(i) + '.sh', mode='w') as outfile:
                    str_to_write = '#!/usr/bin/env bash\n' \
                                   'DIR="/data/Ronald/Results/{}"\n' \
                                   'if [ ! -d ${{DIR}} ]; then mkdir /data/Ronald/Results/{} \n' \
                                   'fi\n' \
                                   'python2.7 /data/Ronald/Pacasus/pacasus.py ' \
                                   '-L /data/Ronald/Results/{}/{}.log ' \
                                   '-c /data/Ronald/Config_files/{}.cfg ' \
                                   '/data/Ronald/Run{}/Run{}_cust_{}.fasta'.format(l[0],
                                                                                    l[0],
                                                                                    l[0],
                                                                                    l[0] + '_' + str(i),
                                                                                    l[0] + '_' + str(i),
                                                                                    ds,
                                                                                    ds,
                                                                                    str(i)
                                                                                    )
                    # str_to_write = '[General]\n' \
                    #                'out_file=/data/Ronald/Results/{}/{}\n' \
                    #                'out_format=FASTA\n' \
                    #                'loglevel=DEBUG\n' \
                    #                'program=palindrome\n' \
                    #                'filetype1=fasta\n' \
                    #                'override_output=T\n' \
                    #                '\n' \
                    #                '[Input]\n' \
                    #                'start_query=0\n' \
                    #                'end_query=0\n' \
                    #                'start_target=0\n' \
                    #                'end_target=0\n' \
                    #                'sequence_step=100000000\n' \
                    #                'query_step=1000000\n' \
                    #                '\n' \
                    #                '[Aligner]\n' \
                    #                'gap_score={}\n' \
                    #                'match_score={}\n' \
                    #                'mismatch_score={}\n' \
                    #                'other_score=-1\n' \
                    #                'any_score=0\n' \
                    #                'matrix_name=DNA-RNA\n' \
                    #                'highest_score=3\n' \
                    #                'lower_limit_score={}\n' \
                    #                'minimum_score={}\n' \
                    #                '\n' \
                    #                '[Filter]\n' \
                    #                'filter_factor={}\n' \
                    #                'query_coverage={}\n' \
                    #                'query_identity={}\n' \
                    #                'relative_score={}\n' \
                    #                'base_score={}\n' \
                    #                '\n' \
                    #                '[Palindrome]\n' \
                    #                'query_coverage_slice={}\n' \
                    #                'minimum_read_length={}\n' \
                    #                '\n' \
                    #                '[Device]\n' \
                    #                'device_number=0\n' \
                    #                'maximum_memory_usage=0.8\n' \
                    #                'number_of_compute_units=0\n' \
                    #                'sub_device=0\n' \
                    #                'limit_length=100000\n' \
                    #                'max_genome_length=100000\n' \
                    #                'recompile=T\n' \
                    #                'short_sequences=F\n' \
                    #                '\n' \
                    #                '[Framework]\n' \
                    #                'language=CUDA\n' \
                    #                '\n' \
                    #                '[OpenCL]\n' \
                    #                'device_type=GPU\n' \
                    #                'platform_name=NVIDIA'.format(l[0],
                    #                                              l[0] + '_' + str(i) + '.fa',
                    #                                              l[1],
                    #                                              l[3],
                    #                                              l[2],
                    #                                              l[5],
                    #                                              l[4],
                    #                                              l[6].replace(',', '.'),
                    #                                              l[7].replace(',', '.'),
                    #                                              l[8].replace(',', '.'),
                    #                                              l[9].replace(',', '.'),
                    #                                              l[10].replace(',', '.'),
                    #                                              l[11].replace(',', '.'),
                    #                                              l[12],
                    #                                              l[13],
                    #                                              )
                    outfile.write(str_to_write)


if __name__ == '__main__':
    main()

"""
'[General]
out_file={}
out_format=FASTA
loglevel=DEBUG
program=palindrome
filetype1=fasta
override_output=T

[Input]
start_query=0
end_query=0
start_target=0
end_target=0
sequence_step=100000000
query_step=1000000

[Aligner]
gap_score={}
match_score={}
mismatch_score={}
other_score={}
any_score=0
matrix_name=DNA-RNA
highest_score=3
lower_limit_score={}
minimum_score={}

[Filter]
filter_factor={}
query_coverage={}
query_identity={}
relative_score={}
base_score={}


[Palindrome]
query_coverage_slice={}
minimum_read_length={}

[Device]
device_number=0
maximum_memory_usage=0.8
number_of_compute_units=0
sub_device=0
limit_length=100000
max_genome_length=100000
recompile=T
short_sequences=F

[Framework]
language=CUDA

[OpenCL]
device_type=GPU
platform_name=NVIDIA
'.format()
"""
