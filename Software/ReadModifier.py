"""
Class ReadModifier. Relies on a SeqRecord object to be initialized.
"""
import bisect
import random
import itertools
from Bio import SeqIO


class ReadModifier:
    def __init__(self, fasta_record, noise=True):
        """Initialize object and check validity input."""
        try:
            if not isinstance(fasta_record, SeqIO.SeqRecord):
                raise TypeError
            else:
                self.fasta_record = fasta_record
            self.do_insert_noise = noise
            self.nucleotides = "ATCG"
            self.nucleotide_pairs = {"A": "T", "T": "A", "C": "G", "G": "C"}
        except TypeError:
            raise TypeError("record has to be of type SeqRecord")

    def append_simple_palindrome(self, min_percentage, max_percentage):
        """Method utilizes randomization to establish length and composition of a single
        palindrome and appends it to original sequence. Calls on multiple other methods."""
        sequence = self.fasta_record.seq
        sequence_length = len(self.fasta_record.seq)

        complete_palindrome, stats = self.create_palindrome(sequence,
                                                            sequence_length,
                                                            min_percentage,
                                                            max_percentage)

        unmodified_palindrome_gc = self.get_gc_percentage(complete_palindrome)

        if self.do_insert_noise:
            modified_palindrome, palindrome_modifications = self.insert_noise(complete_palindrome)

            modified_palindrome_gc = self.get_gc_percentage(modified_palindrome)

            new_read = sequence + modified_palindrome

            stats.extend([unmodified_palindrome_gc, modified_palindrome_gc,
                          palindrome_modifications])
        else:
            new_read = sequence + complete_palindrome

        return new_read, stats

    def append_multi_palindromes(self, min_percentage, max_percentage,
                                 min_multiplier, max_multiplier):
        """Method utilizes randomization to establish length and composition of multiple
        palindromes and appends them to original sequence. Calls on multiple other methods."""
        sequence = self.fasta_record.seq
        sequence_length = len(self.fasta_record.seq)

        complete_palindrome, stats = self.create_palindrome(sequence,
                                                            sequence_length,
                                                            min_percentage,
                                                            max_percentage)

        multiplier = random.randint(min_multiplier, max_multiplier)

        multi_palindrome = multiplier * str(complete_palindrome)

        unmodified_palindrome_gc = self.get_gc_percentage(complete_palindrome)
        if self.do_insert_noise:
            modified_palindrome, palindrome_modifications = self.insert_noise(multi_palindrome)

            modified_palindrome_gc = self.get_gc_percentage(modified_palindrome)

            new_read = sequence + modified_palindrome

            stats.extend([unmodified_palindrome_gc, modified_palindrome_gc,
                          palindrome_modifications])
        else:
            new_read = sequence + multi_palindrome
            stats[3] = multiplier

        return new_read, stats

    def insert_inverted_repeat(self, min_percentage, max_percentage):
        """Insert an inverted repeat into a sequence at random point between 20-80% of the length
        of sequence."""
        sequence = self.fasta_record.seq
        sequence_length = len(self.fasta_record.seq)

        insert_point = random.randint(round(0.2 * sequence_length), round(sequence_length * 0.8))

        complete_palindrome, stats = self.create_palindrome(sequence,
                                                            sequence_length,
                                                            min_percentage,
                                                            max_percentage,
                                                            True,
                                                            insert_point)

        unmodified_palindrome_gc = self.get_gc_percentage(complete_palindrome)

        if self.do_insert_noise:
            modified_palindrome, palindrome_modifications = self.insert_noise(complete_palindrome)

            modified_palindrome_gc = self.get_gc_percentage(modified_palindrome)

            new_read = sequence[:insert_point] + modified_palindrome + sequence[insert_point:]

            stats.extend([unmodified_palindrome_gc, modified_palindrome_gc,
                          palindrome_modifications])
        else:
            new_read = sequence[:insert_point] + complete_palindrome + sequence[insert_point:]

        return new_read, stats

    @staticmethod
    def get_gc_percentage(sequence):
        """Returns the gc percentage of sequence"""
        gc_percentage = len([x for x in sequence[::] if x == "G" or x == "C"])/len(sequence) * 100
        return gc_percentage

    # OBSOLETE!
    # def reversed_complement(self, sequence):
    #     """Takes read and returns the reversed complement."""
    #     reversed_complement = "".join(self.nucleotide_pairs[i] for i in sequence[::-1])
    #     return reversed_complement
    #
    # def insert_noise(self, sequence):
    #     """Method takes a palindrome and inserts random noise in it, trying to copy
    #     real PacBio sequencing noise. Returns the disturbed palindrome along with
    #     the info on what happened. NOTE: Be careful with mixing up positions and
    #     list indices!"""
    #
    #     # Sort out how much and where there should be noise inserted.
    #     sequence_copy = sequence[::]
    #     palindrome_length = len(sequence)
    #     error_percentage = random.randint(10, 30) / 100
    #     # palindrome_length - 1, those are positions rather than indices.
    #     palindrome_positions = list(range(1, palindrome_length - 1, 1))
    #     number_of_errors = round(error_percentage * palindrome_length)
    #     error_positions = random.sample(palindrome_positions, number_of_errors)
    #     sorted_error_positions = sorted(error_positions)
    #
    #     # CLR Pbsim substitution:insertion:deletion = 10:60:30
    #     # Setup randomizer for noise categories.
    #     weighted_categories = [("substitution", 10), ("insertion", 60),
    #                            ("deletion", 30)]
    #     choices, weights = zip(*weighted_categories)
    #     cumulative_distance = list(itertools.accumulate(weights))
    #
    #     # Make bins to store progress.
    #     modified_sequence = ""
    #     restart_pos = 0
    #     modification_bin = []
    #
    #     for position in sorted_error_positions:
    #         # Restart position cannot be 0 due to sequence_copy[restart_pos - 1:position - 1],
    #         # sequence copy[-1:] results in unwanted copy. OBSOLETE!
    #         # if restart_pos == 0:
    #         #     restart_pos = 1
    #         modified_sequence += sequence_copy[restart_pos - 1:position - 1]
    #         draft_number = random.random() * cumulative_distance[-1]
    #         selected_category = choices[bisect.bisect(cumulative_distance, draft_number)]
    #         if selected_category == "substitution":
    #             target = sequence_copy[position - 1]
    #             substitution = "".join(random.choice(self.nucleotides[::].replace(target, "")))
    #             modified_sequence += substitution
    #             restart_pos = position + 1
    #             modification_bin.append(("subs", target, substitution, position))
    #
    #         elif selected_category == "insertion":
    #             neighbour_nucleotide = sequence_copy[position - 1]
    #             non_neighbours = self.nucleotides[::].replace(neighbour_nucleotide, "")
    #             # Chances are greater an insertion is the same as its following nucleotide.
    #             # Ratios are 5:1:1:1 taken from PBSIM.
    #             weighted_nucleotides = [(neighbour_nucleotide, 250),
    #                                     (non_neighbours[0], 83),
    #                                     (non_neighbours[1], 83),
    #                                     (non_neighbours[2], 83)]
    #             nucleotides, weighted_presence = zip(*weighted_nucleotides)
    #             cumulative_distance_2 = list(itertools.accumulate(weighted_presence))
    #             draft_number_2 = random.random() * cumulative_distance_2[-1]
    #             selected_nucleotide = nucleotides[
    #                 bisect.bisect(cumulative_distance_2, draft_number_2)]
    #             modified_sequence += selected_nucleotide
    #             restart_pos = position
    #             modification_bin.append(
    #                 ("ins", position, neighbour_nucleotide, selected_nucleotide))
    #
    #         elif selected_category == "deletion":
    #             restart_pos = position + 1
    #             modification_bin.append(("del", position))
    #
    #     # Paste remainder of sequence.
    #     modified_sequence += sequence_copy[restart_pos - 1:]
    #     return modified_sequence, modification_bin

    def create_palindrome(self, sequence,  sequence_length, min_percentage, max_percentage,
                          inv_rep=False, insert_point=0):
        """Create a palindrome of length x dependant on min_percentage <= rng_percentage
        <= max_percentage of sequence length and return it."""
        percentual_length = random.randint(min_percentage, max_percentage) / 100
        palindrome_length = round(percentual_length * sequence_length)

        if palindrome_length % 2 != 0:
            palindrome_length += 1

        if not inv_rep:
            palindrome_half = sequence[sequence_length - palindrome_length - 1:
                                       sequence_length - 1]

        else:
            palindrome_half = sequence[insert_point - 1: insert_point + palindrome_length - 1]

        palindrome_second_half = palindrome_half.reverse_complement()

        complete_palindrome = palindrome_half + palindrome_second_half

        initial_sequence_length = sequence_length
        stats = [initial_sequence_length, len(complete_palindrome), insert_point, ""]  # Blank for the
        # multiplier

        return complete_palindrome, stats

    def __str__(self):
        return "Original sequence: {} \n Do insert noise: {}".format(str(self.fasta_record.seq),
                                                                     str(self.do_insert_noise))
