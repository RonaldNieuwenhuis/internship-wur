"""
Small script used to get dataset statistics all in once.
"""
import csv
import sys
from Bio import SeqIO
from ReadModifier import ReadModifier
get_gc_percentage = ReadModifier.get_gc_percentage


def open_fasta(input_file):
    """Open fasta and yield record."""
    with open(input_file, mode='r') as file:
        for i in SeqIO.parse(file, 'fasta'):
            yield(i)


def calculate_average_gc(gc):
    """Calculate the average gc percentage of a list consisting of tuples with gc percentage
    and weight"""
    total_gc = 0
    total_length = 0
    for read_gc in gc:
        total_gc += read_gc[1] * read_gc[0]
        total_length += read_gc[1]
    average_gc = total_gc/total_length
    return average_gc

def analyze_fasta(input_file):
    """Parse the headers of the fasta records and put stuff in appropriate bins."""
    counts_bin = {'total_reads': 0,
                  'total_length': 0,
                  'normal': 0,
                  'invrep': 0,
                  'simple': 0,
                  'multi': 0,
                  'subs': 0,
                  'dels': 0,
                  'ins': 0,
                  'multipliers': [],
                  'lengths_before_mod': [],
                  'lengths_after_mod': [],
                  'palindrome_lengths': [],
                  'pbsim_choices': []
                  }
    gc = []
    for record in open_fasta(input_file):
        header = record.id
        #print(header)
        #sys.exit(1)
        header_list = header.split('_')
        seq = record.seq
        gc_perc = get_gc_percentage(seq)
        gc.append([gc_perc, len(seq)])
        counts_bin['total_reads'] += 1
        counts_bin['total_length'] += len(seq)
        counts_bin['subs'] += int(header_list[8])
        counts_bin['dels'] += int(header_list[9])
        counts_bin['ins'] += int(header_list[10])
        counts_bin['pbsim_choices'].append(header_list[-1])
        counts_bin['lengths_before_mod'].append(header_list[3])
        counts_bin['lengths_after_mod'].append(header_list[7])
        if header_list[2] == 'simple':
            counts_bin['simple'] += 1
            counts_bin['palindrome_lengths'].append(header_list[4])
        elif header_list[2] == 'multiple':
            counts_bin['multi'] += 1
            counts_bin['multipliers'].append(header_list[5])
            counts_bin['palindrome_lengths'].append(int(header_list[4]) * int(header_list[5]))
        elif header_list[2] == 'invrep':
            counts_bin['invrep'] += 1
            counts_bin['palindrome_lengths'].append(header_list[4])
        elif header_list[2] == 'normal':
            counts_bin['normal'] += 1
    averaged_gc = calculate_average_gc(gc)
    counts_bin['average_gc'] = averaged_gc
    return counts_bin


def write_results(outfile, results):
    """Write results to file as a csv file."""
    with open(outfile, mode='w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        for feature in sorted(results.items()):
            if type(results.get(feature[0])) is list:
                value = feature[1][::]
                key = str(feature[0])
                row = [key] + value
            else:
                row = [feature[0], feature[1]]
            writer.writerow(row)


def main():
    input_file = sys.argv[1]
    results = analyze_fasta(input_file)
    if not sys.argv[2]:
        outfile = input_file.split('.')[0] + '.csv'
    outfile = sys.argv[2]
    write_results(outfile, results)
    print('Finished')


if __name__ == '__main__':
    main()
