if (!require('ggplot2') | !require(reshape2)) {
  install.packages('ggplot2',repos =  'http://cran.rstudio.com/')
  install.packages('reschape2', repos = 'http://cran.rstudio.com/')
  library('ggplot2')
  library('reshape2')
}

# Main for loop to create a plot for every kmer.
for (n in seq(16, 31)) {
  # Basepath
  path <- '~/Documents/Data/E.coli/'
  n_ch <- as.character(n)
  
  # Loop to get data for every dataset.
  for (i in seq(1:6)) {
    i_ch <- as.character(i)
    full_path <- paste(path, 'Run', i_ch, '/Run', i_ch, '_cust_', n_ch, '.hist', sep = '')
    tmp <- read.table(full_path, header = F)
    new_name <- paste('R', i_ch, sep = '')
    assign(new_name, tmp)
  }

# Reference is also included in every plot.
ref_filename <- paste(path, 'E.Coli_complete_genome_', n_ch, 'mer.hist', sep = '')
ref <- read.table(ref_filename, header = F)


# Transform data.
df <- cbind(R1, R2$V2, R3$V2, R4$V2, R5$V2, R6$V2, ref$V2)
colnames(df)[2:8] <- c('R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'Ref')

# Make a color palette
col_pal <- c('#C46D4A', '#DE8764', '#8ed46e', '#C1FFA1', '#6eabd4', '#A1DEFF', '#000000')

molten_df <- melt(df, id.vars = 1)
colnames(molten_df)[2] <- 'Dataset'
colnames(molten_df)[1] <- 'Frequency'
colnames(molten_df)[3] <- 'Distinct'

molten_df$Color <- factor(molten_df$Dataset, levels = c("R1","R2","R3","R4","R5","R6","Ref"), labels = col_pal)

title <- paste(n_ch, '-mer curves of six datasets and reference', sep = '')
ylabel <- paste(n_ch, 'Number of distinct 21-mers', sep = '')


kmerplot <- ggplot(molten_df, aes(colour = Dataset), size = 0.2) + 
  geom_smooth(aes(Frequency, Distinct), se = F, inherit.aes = T) + 
  #geom_point(aes(Frequency, Distinct), size = 1, position = 'identity') +
  xlim(c(1, 60)) + ylim(c(0, 5e4)) + 
  ggtitle(title) +
  labs(y = ylabel)

kmerplot

outname <- paste('Datasets_', n_ch, 'mers_plot.png', sep = '')

ggsave(filename = outname, plot = kmerplot, path = '/home/ronald/Documents/Data/E.coli/Datasets_stats/Test',
      height = 150, width = 150, units = 'mm')
rm(list = setdiff(ls(), lsf.str()))
}
