# Internship at Wageningen Univeristy and Research

This repository contains the documents, images and collected data (only small summaries) and some R scripts that I used for my internship at Applied Bioinformatics in Wageningen.
It does not contain every snippet of code i.e. bash scripts etc.

**!!Most important!!**
The main program is WGA_PBSIM.py and can be found [here](https://bitbucket.org/RonaldNieuwenhuis/wga_pbsim) 
It has a readme as well.
